package com.page_objects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.drivermanager.DriverManager;

public class Admin_Nationality_Page {
	public static WebDriverWait wait;

	private static Admin_Nationality_Page Admin_Nationality_Instance;

	public static Admin_Nationality_Page getInstance() {

		if (Admin_Nationality_Instance == null) {

			Admin_Nationality_Instance = new Admin_Nationality_Page();
		}
		return Admin_Nationality_Instance;

	}

	@FindBy(id = "menu_admin_viewAdminModule")
	WebElement admin_Module_Locator;
	@FindBy(id = "menu_admin_nationality")
	WebElement nationality_Locator;
	@FindBy(id = "btnAdd")
	WebElement addBtn_Locator;
	@FindBy(id = "nationality_name")
	WebElement name_Locator;
	@FindBy(id = "btnSave")
	WebElement saveBtn_Locator;
	@FindBy(xpath = "//a[contains(text(),'Tamilan')]/../preceding-sibling::td")
	WebElement checkBox_Locator;
	@FindBy(id = "btnDelete")
	WebElement deleteBtn_Locator;
	@FindBy(id = "dialogDeleteBtn")
	WebElement delete_Dialogue_Btn;

	public void nationality() {

		Actions act = new Actions(DriverManager.getDriver());
		act.moveToElement(admin_Module_Locator).perform();

		nationality_Locator.click();
		addBtn_Locator.click();
		name_Locator.sendKeys("Tamilan");
		saveBtn_Locator.click();
		checkBox_Locator.click();
		deleteBtn_Locator.click();
		WebDriverWait wait = new WebDriverWait(DriverManager.getDriver(), 10);
		wait.until(ExpectedConditions.visibilityOf(delete_Dialogue_Btn));
		delete_Dialogue_Btn.click();

	}

}
