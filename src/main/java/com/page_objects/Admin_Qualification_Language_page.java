package com.page_objects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import com.drivermanager.DriverManager;

public class Admin_Qualification_Language_page {

	private static Admin_Qualification_Language_page admin_qualification_languate_instance;

	public static Admin_Qualification_Language_page getInstance() {

		if (admin_qualification_languate_instance == null) {

			admin_qualification_languate_instance = new Admin_Qualification_Language_page();
		}
		return admin_qualification_languate_instance;

	}

	@FindBy(id = "menu_admin_viewAdminModule")
	WebElement admin_Module_Locator;
	@FindBy(id = "menu_admin_Qualifications")
	WebElement qualification_Module_Locator;
	@FindBy(id = "menu_admin_viewLanguages")
	WebElement language_Locator;

	@FindBy(id = "btnAdd")
	WebElement addBtn_Locator;
	@FindBy(id = "language_name")
	WebElement name_Locator;
	@FindBy(id = "btnSave")
	WebElement saveBtn_Locator;
	@FindBy(xpath = "//a[contains(text(),'TAMIL')]/../preceding-sibling::td")
	WebElement checkBox_Locator;
	@FindBy(id = "btnDel")
	WebElement deleteBtn_Locator;

	public void language() {

		Actions act = new Actions(DriverManager.getDriver());
		act.moveToElement(admin_Module_Locator).perform();
		act.moveToElement(qualification_Module_Locator).perform();
		language_Locator.click();
		addBtn_Locator.click();
		name_Locator.sendKeys("TAMIL");
		saveBtn_Locator.click();
		checkBox_Locator.click();
		deleteBtn_Locator.click();

	}

}
