package com.page_objects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import com.drivermanager.DriverManager;

public class Admin_Qualification_License {
	

	private static Admin_Qualification_License admin_qualificaation_liscence;

	public static Admin_Qualification_License getInstance() {

		if (admin_qualificaation_liscence == null) {

			admin_qualificaation_liscence = new Admin_Qualification_License();
		}
		return admin_qualificaation_liscence;

	}

	@FindBy(id = "menu_admin_viewAdminModule")
	WebElement admin_Module_Locator;
	@FindBy(id = "menu_admin_Qualifications")
	WebElement qualification_Module_Locator;
	@FindBy(id = "menu_admin_viewLicenses")
	WebElement license_Module_Locator;
	@FindBy(id = "btnAdd")
	WebElement addBtn_Locator;
	@FindBy(id = "license_name")
	WebElement name_Locator;
	@FindBy(id = "btnSave")
	WebElement saveBtn_Locator;
	@FindBy(xpath = "//a[contains(text(),'ISTQB')]/../preceding-sibling::td")
	WebElement checkBox_Locator;
	@FindBy(id = "btnDel")
	WebElement deleteBtn_Locator;

	public void license() {

		Actions act = new Actions(DriverManager.getDriver());
		act.moveToElement(admin_Module_Locator).perform();
		act.moveToElement(qualification_Module_Locator).perform();
		license_Module_Locator.click();
		addBtn_Locator.click();
		name_Locator.sendKeys("ISTQB");
		saveBtn_Locator.click();
		checkBox_Locator.click();
		deleteBtn_Locator.click();

	}

}
