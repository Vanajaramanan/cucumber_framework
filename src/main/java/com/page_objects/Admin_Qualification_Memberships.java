package com.page_objects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.drivermanager.DriverManager;




public class Admin_Qualification_Memberships {
	
	private static Admin_Qualification_Memberships admin_qualificaation_Membership_instance;

	public static Admin_Qualification_Memberships getInstance() {

		if (admin_qualificaation_Membership_instance == null) {

			admin_qualificaation_Membership_instance = new Admin_Qualification_Memberships();
		}
		return admin_qualificaation_Membership_instance;

	}



	@FindBy(id = "enu_admin_viewAdminModule")
//	@FindBy(id = "menu_admin_viewAdminModule")

	WebElement admin_Module_Locator;
	@FindBy(id = "menu_admin_Qualifications")
	WebElement qualification_Module_Locator;
	@FindBy(id="menu_admin_membership") WebElement membership_Module_Locator;
	@FindBy(id="btnAdd") WebElement addBtn_Locator;
	@FindBy(id="membership_name") WebElement name_Locator;
	@FindBy(id="btnSave") WebElement saveBtn_Locator;
	@FindBy(xpath="//a[contains(text(),'ISTQB12')]/../preceding-sibling::td") WebElement checkBox_Locator;
	@FindBy(id="btnDelete") WebElement deleteBtn_Locator;
	@FindBy(id="dialogDeleteBtn") WebElement delete_Dialogue_Btn;
	

	
	public void memberShip() {

		
	Actions	act = new Actions(DriverManager.getDriver());
		act.moveToElement(admin_Module_Locator).perform();
		act.moveToElement(qualification_Module_Locator).perform();
		membership_Module_Locator.click();
		addBtn_Locator.click();
		name_Locator.sendKeys("ISTQB12");
		saveBtn_Locator.click();
		checkBox_Locator.click();
		deleteBtn_Locator.click();
		WebDriverWait wait = new WebDriverWait(DriverManager.getDriver(),10);
		wait.until(ExpectedConditions.visibilityOf(delete_Dialogue_Btn));
		delete_Dialogue_Btn.click();
		

	}
	
}
