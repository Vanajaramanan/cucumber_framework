package com.page_objects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import com.drivermanager.DriverManager;



public class Admin_Qualification_skills {
	

	private static Admin_Qualification_skills admin_qualificaton_skilss_instance;

	public static Admin_Qualification_skills getInstance() {

		if (admin_qualificaton_skilss_instance == null) {

			admin_qualificaton_skilss_instance = new Admin_Qualification_skills();
		}
		return admin_qualificaton_skilss_instance;

	}
	
	

	@FindBy(id = "menu_admin_viewAdminModule")
	WebElement admin_Module_Locator;
	@FindBy(id = "menu_admin_Qualifications")
	WebElement qualification_Module_Locator;
	@FindBy(id="menu_admin_viewSkills") 
	WebElement skills_Locator;
	@FindBy(id="btnAdd") WebElement addBtn_Locator;
	@FindBy(id="skill_name") WebElement nameField_Locator;
	@FindBy(id="skill_description") WebElement descriptionField_Locator;
	@FindBy(id="btnSave") WebElement saveBtn_Locator;
	@FindBy(xpath="//a[contains(text(),'Testing')]/../preceding-sibling::td//input")  
	WebElement checkBox_Locator;
	@FindBy(id="btnDel") WebElement deleteBtn_Locator;
	
	
	
	
	
	
	
	
	
	public void skills() {
		 Actions act = new Actions(DriverManager.getDriver());
		act.moveToElement(admin_Module_Locator).perform();
		act.moveToElement(qualification_Module_Locator).perform();
		skills_Locator.click();
		addBtn_Locator.click();
		nameField_Locator.sendKeys("Testing");
		descriptionField_Locator.sendKeys("Automation Selenium ");
		saveBtn_Locator.click();
		checkBox_Locator.click();
		deleteBtn_Locator.click();
		System.out.println("   ");
		
	}
	
	
	
	
	
}
