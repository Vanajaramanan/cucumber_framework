package com.page_objects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.drivermanager.DriverManager;

public class Admin_job_WorkShifts1 {

	public static WebDriverWait wait;

	private static Admin_job_WorkShifts1 AdminjobworkshiftInstance;



	public static Admin_job_WorkShifts1 getInstance() {

		if (AdminjobworkshiftInstance == null) {

			AdminjobworkshiftInstance = new Admin_job_WorkShifts1();
		}
		return AdminjobworkshiftInstance;

	}

	Select select;
	@FindBy(id = "menu_admin_viewAdminModule")
	WebElement admin_Module_Locator;
	@FindBy(id = "menu_admin_Job")
	WebElement job_Module_Locator;
	@FindBy(id = "menu_admin_workShift")
	WebElement work_Shift_Locator;
	@FindBy(id = "btnAdd")
	WebElement addBtn_Locator;
	@FindBy(id = "workShift_name")
	WebElement work_Shift_TxtField;
	@FindBy(name = "workShift[workHours][from]")
	WebElement workHours_From;
	@FindBy(name = "workShift[workHours][to]")
	WebElement workHours_To;
	@FindBy(name = "workShift[availableEmp][]")
	WebElement assigned_Employee;

	@FindBy(id = "btnAssignEmployee")
	WebElement add_Employee;
	@FindBy(id = "btnSave")
	WebElement saveBtn_Locator;
	@FindBy(xpath = "//a[contains(text(),'Freeman Shift')]//preceding::td//input")
	WebElement checkBox_Locator;
	@FindBy(id = "btnDelete")
	WebElement deleteBtn_Locator;
	@FindBy(id = "dialogDeleteBtn")
	WebElement deleteBtn_Dialog_Locator;

	public void workShift() {
		Actions act = new Actions(DriverManager.getDriver());
		act.moveToElement(admin_Module_Locator).perform();
		act.moveToElement(job_Module_Locator).perform();
		work_Shift_Locator.click();
		addBtn_Locator.click();
		work_Shift_TxtField.sendKeys("Freeman Shift ");

		select = new Select(assigned_Employee);
		for (int i = 1; i < 5; i++) {
			select.selectByIndex(i);
			add_Employee.click();
		}
		saveBtn_Locator.click();
		wait = new WebDriverWait(DriverManager.getDriver(),15);
		wait.until(ExpectedConditions.visibilityOf(checkBox_Locator));
		checkBox_Locator.click();
		deleteBtn_Locator.click();
		wait.until(ExpectedConditions.visibilityOf(deleteBtn_Dialog_Locator));
		deleteBtn_Dialog_Locator.click();

	}

}
