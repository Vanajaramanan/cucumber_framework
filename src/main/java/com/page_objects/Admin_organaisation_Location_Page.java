package com.page_objects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.drivermanager.DriverManager;



public class Admin_organaisation_Location_Page {
	
	public static WebDriverWait wait;

	private static Admin_organaisation_Location_Page Admin_organisation_location_instance;

	public static Admin_organaisation_Location_Page getInstance() {

		if (Admin_organisation_location_instance == null) {

			Admin_organisation_location_instance = new Admin_organaisation_Location_Page();
		}
		return Admin_organisation_location_instance;

	}
Select select;
	
	
	@FindBy(id="menu_admin_viewAdminModule") WebElement admin_Module_Locator;
	@FindBy(id="menu_admin_Organization") WebElement organization_Module_Locator;
	@FindBy(id="menu_admin_viewLocations") WebElement location_Menu_Locator;
	@FindBy(id="btnAdd") WebElement addBtn_Locator;
	@FindBy(id="location_name") WebElement location_TxtField;
	@FindBy(id="location_country") WebElement location_Country_DropDown;
	@FindBy(id="location_province") WebElement state_Province;
	@FindBy(id="location_city") WebElement city_Location;
	@FindBy(id="location_address") WebElement address_TxtField;
	@FindBy(id="location_zipCode") WebElement zipCode_txtField;
	@FindBy(id="location_phone") WebElement phone_TxtField;
	@FindBy(id="location_notes") WebElement notes_TxtField;
	@FindBy(id="btnSave") WebElement saveBtn_Locator;
	@FindBy(id="searchLocation_name") WebElement search_LocationField;
	@FindBy(id="btnSearch") WebElement searchBtn_Locator;
	@FindBy(xpath="//td[contains(text(),'Miami')]//preceding-sibling::td//input") 
	WebElement checkBox_Locator;
	@FindBy(id = "btnDelete")
	WebElement deleteBtn_Locator;
	@FindBy(id = "dialogDeleteBtn")
	WebElement deleteBtn_Dialog_Locator;
	
	
	
	
	public void location() 
	{
	Actions	act = new Actions(DriverManager.getDriver());
		act.moveToElement(admin_Module_Locator).perform();
		act.moveToElement(organization_Module_Locator).perform();
		location_Menu_Locator.click();
		addBtn_Locator.click();
		location_TxtField.sendKeys("USA");
		select = new Select(location_Country_DropDown);
		select.selectByIndex(3);
		state_Province.sendKeys("Florida");
		city_Location.sendKeys("Miami");
		address_TxtField.sendKeys("11, Miami Main Road");
		zipCode_txtField.sendKeys("77449");
		phone_TxtField.sendKeys("8998762356");
		notes_TxtField.sendKeys("Automation Company");
		saveBtn_Locator.click();
		checkBox_Locator.click();
		deleteBtn_Locator.click();
		deleteBtn_Dialog_Locator.click();
		
		
	}
	
	
	
	
	
	
}
