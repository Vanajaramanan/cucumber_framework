package com.page_objects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.drivermanager.DriverManager;


public class Admin_organisation_General_Information_Page {

		
		public static WebDriverWait wait;

		private static Admin_organisation_General_Information_Page admin_organisation_general_info_instance;

		public static Admin_organisation_General_Information_Page getInstance() {

			if (admin_organisation_general_info_instance == null) {

				admin_organisation_general_info_instance = new Admin_organisation_General_Information_Page();
			}
			return admin_organisation_general_info_instance;

		}
	
	Select select;

	@FindBy(id="menu_admin_viewAdminModule") WebElement admin_Module_Locator;
	@FindBy(id="menu_admin_Organization") WebElement organization_Module_Locator;
	@FindBy(id="menu_admin_viewOrganizationGeneralInformation") WebElement general_Information;
	@FindBy(id="btnSaveGenInfo") WebElement editBtn_Locator;
	@FindBy(id="organization_name") WebElement organizational_name;
	@FindBy(id="organization_phone") WebElement organizational_Phone;
	@FindBy(id="organization_email") WebElement organizational_Mail;
	@FindBy(id="organization_street1") WebElement organizational_Address;
	@FindBy(id="organization_city") WebElement organizational_City;
	@FindBy(id="organization_zipCode") WebElement organizational_ZipCode;
	@FindBy(id="organization_note") WebElement organizational_Note;
	@FindBy(id="organization_country") WebElement organizational_Country;
	@FindBy(id="btnSaveGenInfo") WebElement saveBtn_Locator;
	
	
	
	
	
	
	
	public void general_Information()
	{
	Actions 	act = new Actions(DriverManager.getDriver());
		act.moveToElement(admin_Module_Locator).perform();
		act.moveToElement(organization_Module_Locator).perform();
		general_Information.click();
		editBtn_Locator.click();
		organizational_name.clear();
		organizational_name.sendKeys("RAM COMPANY");
		organizational_Phone.clear();
		organizational_Phone.sendKeys("9876543210");
		organizational_Mail.clear();
		organizational_Mail.sendKeys("vanju33@gmail.com");
		organizational_Address.clear();
		organizational_Address.sendKeys("11, Anjali Street");
		organizational_City.clear();
		organizational_City.sendKeys("Chennai");
		organizational_ZipCode.clear();
		organizational_ZipCode.sendKeys("600088");
		organizational_Note.clear();
		organizational_Note.sendKeys("Ram Automation Works");
		select = new Select(organizational_Country);
		select.selectByVisibleText("India");
		
		saveBtn_Locator.click();
		

	}

		
	
}
