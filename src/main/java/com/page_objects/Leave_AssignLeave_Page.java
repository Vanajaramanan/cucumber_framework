package com.page_objects;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.drivermanager.DriverManager;



public class Leave_AssignLeave_Page {
	
	private static Leave_AssignLeave_Page  Leave_AssignLeave_instance;

	public static Leave_AssignLeave_Page getInstance() {

		if (Leave_AssignLeave_instance == null) {

			Leave_AssignLeave_instance = new Leave_AssignLeave_Page();
		}
		return Leave_AssignLeave_instance;

	}
	@FindBy(xpath = "//*[@id=\"menu_leave_viewLeaveModule\"]/b")
	WebElement Leavelocator;
	@FindBy(xpath = "//*[@id=\"menu_leave_assignLeave\"]")
	WebElement Assignleavelocator;
	@FindBy(xpath = "//*[@id=\"assignleave_txtEmployee_empName\"]")
	WebElement empnamelocator;
	@FindBy(xpath = "/html/body/div[6]/ul/li")
	WebElement rebecalocator;
	@FindBy(xpath = "//*[@id=\"assignleave_txtLeaveType\"]")
	WebElement leavetypelocator;
	@FindBy(xpath = "//*[@id=\"assignleave_txtFromDate\"]")
	WebElement fromdatelocator;
	@FindBy(xpath = "//*[@id=\"frmLeaveApply\"]/fieldset/ol/li[4]/img")
	WebElement calendarloglocator;
	@FindBy(xpath = "//*[@id=\"assignleave_txtToDate\"]")
	WebElement Todatelocator;
	@FindBy(xpath = "//*[@id=\"frmLeaveApply\"]/fieldset/ol/li[5]/img")
	WebElement Tocalendarlogo;
	@FindBy(xpath = "//*[@id=\"assignBtn\"]")
	WebElement assignbttnlocator;
	@FindBy(xpath = "//*[@id=\"confirmOkButton\"]")
	WebElement assignokbttnlocator;

	

	public void assignLeave() {

	Actions	act = new Actions(DriverManager.getDriver());
		act.moveToElement(Leavelocator).perform();
		Assignleavelocator.click();
		empnamelocator.sendKeys("Lis");
		rebecalocator.click();
		Select leavetyper = new Select(leavetypelocator);
		leavetyper.selectByVisibleText("CAN - Matternity");
		fromdatelocator.clear();
		fromdatelocator.sendKeys("2022-01-25");
		calendarloglocator.click();
		Todatelocator.clear();
		Todatelocator.sendKeys("2022-01-25");
		Tocalendarlogo.click();
		JavascriptExecutor js = (JavascriptExecutor) DriverManager.getDriver();

		js.executeScript("arguments[0].scrollIntoView();", assignbttnlocator);
		assignbttnlocator.click();
		assignokbttnlocator.click();

	}

}
