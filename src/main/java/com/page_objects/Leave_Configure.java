package com.page_objects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.drivermanager.DriverManager;



public class Leave_Configure {

	private static Leave_Configure Leave_Configure_instance;

	public static Leave_Configure getInstance() {

		if (Leave_Configure_instance == null) {

			Leave_Configure_instance = new Leave_Configure();
		}
		return Leave_Configure_instance;

	}

	@FindBy(id = "menu_leave_viewLeaveModule")
	WebElement leaveLocator;
	@FindBy(xpath = "//*[@id=\"menu_leave_Configure\"]")
	WebElement configurelocator;
	@FindBy(xpath = "//*[@id=\"menu_leave_leaveTypeList\"]")
	WebElement leavetypelocator;
	@FindBy(xpath = "//*[@id=\"btnAdd\"]")
	WebElement addbttnlocator;
	@FindBy(xpath = "//*[@id=\"leaveType_txtLeaveTypeName\"]")
	WebElement namelocator;
	@FindBy(xpath = "//*[@id=\"leaveType_excludeIfNoEntitlement\"]")
	WebElement checkboxlocator;
	@FindBy(xpath = "//*[@id=\"saveButton\"]")
	WebElement savebttnlocator;
	@FindBy(xpath = " //td[contains(a,'Sick Leave')]//preceding-sibling::td//input")
	WebElement sickleaveboxlocator;
	@FindBy(xpath = "//*[@id=\"btnDelete\"]")
	WebElement deletbttnlocator;
	@FindBy(xpath = "//*[@id=\"dialogDeleteBtn\"]")
	WebElement deletokbttnlocator;

	public void addingLeaveType() {

		Actions act = new Actions(DriverManager.getDriver());
		act.moveToElement(leaveLocator).perform();
		act.moveToElement(configurelocator).perform();
		leavetypelocator.click();
		addbttnlocator.click();
		namelocator.sendKeys("Sick Leave");
		checkboxlocator.click();
		savebttnlocator.click();
		sickleaveboxlocator.click();
		deletbttnlocator.click();
		deletokbttnlocator.click();

	}
}
