package com.page_objects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.drivermanager.DriverManager;


public class Leave_Reports  {
	

	private static Leave_Reports  Leave_Reports_instance;

	public static Leave_Reports getInstance() {

		if (Leave_Reports_instance == null) {

			Leave_Reports_instance = new Leave_Reports();
		}
		return Leave_Reports_instance;

	}
	

	@FindBy(id = "menu_leave_viewLeaveModule")
	WebElement leaveLocator;
	@FindBy(xpath = "//*[@id=\"menu_leave_Reports\"]")
	WebElement reportlocator;
	@FindBy(xpath = "//*[@id=\"menu_leave_viewLeaveBalanceReport\"]")
	WebElement leaveusagereport;
	@FindBy(xpath = "//*[@id=\"leave_balance_report_type\"]")
	WebElement generateforlocator;
	@FindBy(xpath = "//*[@id=\"viewBtn\"]")
	WebElement viewbttnlocator;

	

	public void viewleave() {
		Actions action = new Actions(DriverManager.getDriver());
		action.moveToElement(leaveLocator).perform();
		action.moveToElement(reportlocator).perform();
		leaveusagereport.click();
		Select reports = new Select(generateforlocator);
		reports.selectByVisibleText("Leave Type");
		viewbttnlocator.click();

	}
}
