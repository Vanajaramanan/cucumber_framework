package com.page_objects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.drivermanager.DriverManager;

public class LogOutUser {

	private static LogOutUser logoutInstance;

	private LogOutUser() {
		// TODO Auto-generated constructor stub
	}

	public static LogOutUser getInstance() {

		if (logoutInstance == null) {

			logoutInstance = new LogOutUser();
		}
		return logoutInstance;

	}

	@FindBy(xpath = "//a[@id='welcome']")
	private static WebElement welcomeLocator;
	@FindBy(xpath = "//a[text()='Logout']")
	private static WebElement logoutLocator;

	public static void logOutFunc() {
		welcomeLocator.click();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {

			e.printStackTrace();
		}
		logoutLocator.click();

		DriverManager.getDriver().quit();

	}
	// driver.quit().then(function () {
	// callback();
	//

}
