package com.page_objects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage {

	private static LoginPage loginInstance;

	private LoginPage() {
		// TODO Auto-generated constructor stub
	}

	public static LoginPage getInstance() {

		if (loginInstance == null) {

			loginInstance = new LoginPage();

		}
		return loginInstance;

	}

	@FindBy(xpath = "//*[@id=\"app\"]/div[1]/div/div[1]/div/div[2]/div[2]/form/div[1]/div/div[2]/input  ")
	private WebElement usernamelocator;

	@FindBy(xpath = "//*[@id=\"app\"]/div[1]/div/div[1]/div/div[2]/div[2]/form/div[2]/div/div[2]/input  ")
	private WebElement passwordlocator;

	@FindBy(xpath = "//*[@id=\"app\"]/div[1]/div/div[1]/div/div[2]/div[2]/form/div[3]/button")
	private WebElement loginbttnlocator;

	/*
	 * public WebElement getUsernamelocator() { return usernamelocator; }
	 * 
	 * public WebElement getPasswordlocator() { return passwordlocator; }
	 * 
	 * public WebElement getLoginbttnlocator() { return loginbttnlocator; }
	 */

	public void enterUserName(String username) {

		usernamelocator.sendKeys(username);

	}

	public void enterPassword(String password) {

		passwordlocator.sendKeys(password);
	}

	public void clickLoginButton() {
		loginbttnlocator.click();

	}

}
