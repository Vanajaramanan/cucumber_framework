package com.step_definitions;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;


import com.page_objects.Admin_organaisation_Location_Page;
import com.utils.CommonUtils;
import io.cucumber.java.en.Given;

public class Admin_Organisation_location_Def {

	private static final Logger LOGGER = LogManager.getLogger(Admin_Organisation_location_Def.class.getName());

	@Given("^Admin user set the Organissation location$")
	public void admin_user_set_the_Organissation_location() throws Throwable {
		try {

			Admin_organaisation_Location_Page.getInstance().location();

		} catch (Exception e) {
			// TODO: handle exception
			LOGGER.error("Admin can able to add Organissation");
			
			Assert.fail();

		}

	}

}
