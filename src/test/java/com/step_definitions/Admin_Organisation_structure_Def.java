package com.step_definitions;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;

import com.page_objects.Admin_organisation_Structure;
import com.utils.CommonUtils;

import io.cucumber.java.en.Given;

public class Admin_Organisation_structure_Def {
	private static final Logger LOGGER = LogManager.getLogger(Admin_Organisation_structure_Def.class.getName());

	@Given("^Admin can able to check organisation structure$")
	public void admin_can_able_to_check_organisation_structure() throws Throwable {

		try {

			Admin_organisation_Structure.getInstance().structure();
		} catch (Exception e) {
			// TODO: handle exception
			LOGGER.error(" Adin can able to add organisation structure");
			
			Assert.fail();

		}
	}

}
