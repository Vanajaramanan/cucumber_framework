package com.step_definitions;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;

import com.page_objects.Admin_Qualification_Education;
import com.utils.CommonUtils;

import io.cucumber.java.en.Given;

public class Admin_Qualification_Education_Def {

	private static final Logger LOGGER = LogManager.getLogger(Admin_Qualification_Education_Def.class.getName());

	@Given("^Admin can able to check qualification educatioon$")
	public void admin_can_able_to_check_qualification_educatioon() throws Throwable {

		try {

			Admin_Qualification_Education.getInstance().education();

		} catch (Exception e) {
			// TODO: handle exception
			LOGGER.error(" Admin qualification educcation erroR");
			
			Assert.fail();

		}

	}

}
