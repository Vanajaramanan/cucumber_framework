package com.step_definitions;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;

import com.page_objects.Admin_Qualification_Memberships;
import com.utils.CommonUtils;

import io.cucumber.java.en.Given;

public class Admin_Qualification_Membership_def {

	private static final Logger LOGGER = LogManager.getLogger(Admin_Qualification_Membership_def.class.getName());

	@Given("^admin can able to check  the membersip$")
	public void admin_can_able_to_check_the_membersip() throws Throwable {

		try {
			Admin_Qualification_Memberships.getInstance().memberShip();
		} catch (Exception e) {
			// TODO: handle exception
			LOGGER.error(" Admin can able to chcek qualification Membership ");
			CommonUtils.getInstance().takeScreenshot();
			Assert.fail();

		}

	}
}
