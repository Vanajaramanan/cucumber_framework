package com.step_definitions;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Base64;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter;
import com.constants.Constants;
import com.drivermanager.DriverManager;
import com.page_objects.Admin_Configuration_Email;
import com.page_objects.Admin_Job_Jobtitle_Page;
import com.page_objects.Admin_Nationality_Page;
import com.page_objects.Admin_Qualification_Education;
import com.page_objects.Admin_Qualification_Language_page;
import com.page_objects.Admin_Qualification_License;
import com.page_objects.Admin_Qualification_Memberships;
import com.page_objects.Admin_Qualification_skills;

import com.page_objects.Admin_job_WorkShifts1;
import com.page_objects.Admin_organaisation_Location_Page;
import com.page_objects.Admin_organisation_General_Information_Page;
import com.page_objects.Admin_organisation_Structure;
import com.page_objects.LeavePage;
import com.page_objects.Leave_AssignLeave_Page;
import com.page_objects.Leave_Configure;
import com.page_objects.Leave_LeaveList;
import com.page_objects.Leave_Reports;
import com.page_objects.LogOutUser;
import com.page_objects.LoginPage;
import com.page_objects.Performance_Configure_Kpis;
import com.page_objects.Performance_Configure_Trackers;
import com.page_objects.Performane_Myreview_Myreview_page;
import com.utils.CommonUtils;

import io.cucumber.java.After;
import io.cucumber.java.AfterStep;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;

public class Common_Step_Definition {
	// byte[] screenshotTaken =
	// ((TakesScreenshot)DriverManager.getDriver()).getScreenshotAs(OutputType.BYTES);
	// scenario.attach(imgpath, "image/png", (dtf.format(now)));

	public static String scenarioName = null;

	private static final Logger LOGGER = LogManager.getLogger(Common_Step_Definition.class.getName());

	@Before()
	public void beforeScenario(Scenario scenario) {

		LOGGER.info("Exceution Started");
		LOGGER.info("After Execution started");
		try {
			scenarioName = scenario.getName();
			LOGGER.info("Instantiation the CommonUtils");

			LOGGER.info("Loading the properties file");
			CommonUtils.getInstance().loadProperties();
			// if (DriverManager.getDriver() == null) {

			DriverManager.launchBrowser();
			Common_Step_Definition.initWebElement();
			login();
			// }

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

	@After
	public void logout() {
		LogOutUser.logOutFunc();

	}

	private void login() {
		DriverManager.getDriver().get(Constants.APPURL);

		LoginPage.getInstance().enterUserName(Constants.USERNAME);
		LoginPage.getInstance().enterPassword(Constants.PASSWORD);
		LoginPage.getInstance().clickLoginButton();

	}

	public static void initWebElement() {

		PageFactory.initElements(DriverManager.getDriver(), LoginPage.getInstance());

		PageFactory.initElements(DriverManager.getDriver(), Admin_Job_Jobtitle_Page.getInstance());
		PageFactory.initElements(DriverManager.getDriver(), Admin_organaisation_Location_Page.getInstance());
		PageFactory.initElements(DriverManager.getDriver(), Admin_organisation_General_Information_Page.getInstance());
		PageFactory.initElements(DriverManager.getDriver(), Admin_Nationality_Page.getInstance());
		PageFactory.initElements(DriverManager.getDriver(), Admin_Configuration_Email.getInstance());
		PageFactory.initElements(DriverManager.getDriver(), LogOutUser.getInstance());
		PageFactory.initElements(DriverManager.getDriver(), Admin_Nationality_Page.getInstance());
		PageFactory.initElements(DriverManager.getDriver(), Admin_job_WorkShifts1.getInstance());
		PageFactory.initElements(DriverManager.getDriver(), Admin_organisation_Structure.getInstance());
		PageFactory.initElements(DriverManager.getDriver(), Admin_Qualification_Education.getInstance());
		PageFactory.initElements(DriverManager.getDriver(), Admin_Qualification_License.getInstance());
		PageFactory.initElements(DriverManager.getDriver(), Admin_Qualification_Memberships.getInstance());
		PageFactory.initElements(DriverManager.getDriver(), Admin_Qualification_skills.getInstance());

		PageFactory.initElements(DriverManager.getDriver(), Admin_Qualification_Language_page.getInstance());
		PageFactory.initElements(DriverManager.getDriver(), Leave_AssignLeave_Page.getInstance());
		PageFactory.initElements(DriverManager.getDriver(), Leave_Configure.getInstance());
		PageFactory.initElements(DriverManager.getDriver(), Leave_LeaveList.getInstance());
		PageFactory.initElements(DriverManager.getDriver(), Leave_Reports.getInstance());
		PageFactory.initElements(DriverManager.getDriver(), LeavePage.getInstance());
		PageFactory.initElements(DriverManager.getDriver(), Performane_Myreview_Myreview_page.getInstance());
		PageFactory.initElements(DriverManager.getDriver(), Performance_Configure_Kpis.getInstance());
		PageFactory.initElements(DriverManager.getDriver(), Performance_Configure_Trackers.getInstance());
		

	}

	@AfterStep
	public static void attachScreenshot(Scenario scenario) throws IOException {

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();
		System.out.println(dtf.format(now));

		if (scenario.isFailed()) {
//			 String Base64StringofScreenshot = null;
//			
//			 TakesScreenshot scr = ((TakesScreenshot) DriverManager.getDriver());
//			
//			 File source = scr.getScreenshotAs(OutputType.FILE);
//			 String destImg = (System.getProperty("user.dir") + "/Screenshot/");
//			 //String destImg = "./reports/screenshots"+(dtf.format(now)+ ".png");
//			 File destination = new File(destImg);
//			
//			 try {
//			 FileUtils.copyFile(source, destination);
//			
//			 } catch (IOException e1) {
//			 // TODO Auto-generated catch block
//			 e1.printStackTrace();
//			 }
//			 byte[] fileContent;
//			 try {
//			 fileContent = FileUtils.readFileToByteArray(source);
//			
//			 Base64StringofScreenshot = "data:image/png;base64," + Base64.getEncoder().encodeToString(fileContent);
//			
//			 } catch (IOException e) {
//			 e.printStackTrace();
//			 }
//			 scenario.attach(Base64StringofScreenshot, "image/png", (dtf.format(now)));
			// return Base64StringofScreenshot;
			// TakesScreenshot ts = (TakesScreenshot)driver;
			// File source = ts.getScreenshotAs(OutputType.FILE);
			// FileUtils.copyFile(source, new File("./Screenshots/Screen.png"));
			// System.out.println("the Screenshot is taken");
			
		//	String path ="cucumber_framework\\target\\Screenshot ";
			//
			
			byte[] screenshotTaken = ((TakesScreenshot) DriverManager.getDriver()).getScreenshotAs(OutputType.BYTES);
			scenario.attach(screenshotTaken, "image/png", (dtf.format(now)));

			// File screenshot = ((TakesScreenshot)
			// DriverManager.getDriver()).getScreenshotAs(OutputType.FILE);
			// byte[] readFileToByteArray = FileUtils.readFileToByteArray(screenshot);

			// scenario.attach(screenshotTaken, "image/png", (dtf.format(now)));

		}

	}

}
