package com.step_definitions;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.page_objects.Leave_Reports;

import io.cucumber.java.en.Given;

public class LeaveReports_Def {
	private static final Logger LOGGER = LogManager.getLogger(LeaveReports_Def.class.getName());


	
	@Given("^Navigate to leave reports and view$")
	public void navigate_to_leave_reports_and_view() {
		
		try {
			 Leave_Reports.getInstance().viewleave();
			
		} catch (Exception e) {
			// TODO: handle exception
			LOGGER.error(" user can t able to leave assign ");
		}
	  
	}
}
