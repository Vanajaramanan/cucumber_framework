package com.step_definitions;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;

import com.page_objects.Leave_AssignLeave_Page;

import io.cucumber.java.en.Given;

public class Leave_AssignLeave_Def {
	
	private static final Logger LOGGER = LogManager.getLogger(Leave_AssignLeave_Def.class.getName());

	@Given("^Enter the details and assign leave$")
	public void enter_the_details_and_assign_leave() {
		
		try {
			Leave_AssignLeave_Page.getInstance().assignLeave();
			
			
		} catch (Exception e) {
			// TODO: handle exception
			
LOGGER.error(" user can t able to leave assign ");
			
			Assert.fail();
		}
	
		
	}
}
