package com.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.support.PageFactory;

import com.constants.Constants;
import com.drivermanager.DriverManager;

import com.page_objects.LoginPage;
import com.step_definitions.Common_Step_Definition;

import io.cucumber.java.Scenario;

public class CommonUtils {

	private static CommonUtils commonutilsinstance = null;

	private CommonUtils() {

	}

	public static CommonUtils getInstance() {

		if (commonutilsinstance == null)
			commonutilsinstance = new CommonUtils();
		return commonutilsinstance;

	}

	public void loadProperties() {

		Properties properties = new Properties();

		try {

			properties.load(getClass().getResourceAsStream("/config.properties"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		Constants.APPURL = properties.getProperty("APPURL");
		Constants.BROWSER = properties.getProperty("BROWSER");
		Constants.USERNAME = properties.getProperty("USERNAME");
		Constants.PASSWORD = properties.getProperty("PASSWORD");
		Constants.EMPNAME = properties.getProperty("empname");

	}

	public void takeScreenshot() throws IOException {
		File screenshot = ((TakesScreenshot) DriverManager.getDriver()).getScreenshotAs(OutputType.FILE);
	//	byte[] readFileToByteArray = FileUtils.readFileToByteArray(screenshot);
		
		try {

			FileUtils.copyFile(screenshot, new File("Screenshot\\" + Common_Step_Definition.scenarioName + ".png"));
		//.	scenario.attach(readFileToByteArray, "image/png", "iamgeeee");
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		}

	}

}
